package ua.home;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.actions;

public class MainPage {

    public static ElementsCollection tasks = $$("#todo-list>li");
    public static ElementsCollection visibleTasks = tasks.filter(visible);

    @Step
    public static void clearCompleted(){
        $("#clear-completed").click();
    }

    @Step
    public static void addTasks(String... texts){
        for (String text: texts){
            $("#new-todo").setValue(text).pressEnter();
        }
    }

    @Step
    public static void assertItemsLeft(int itemsLeft){
        $("#todo-count strong").shouldBe(exactText(String.valueOf(itemsLeft)));
    }

    @Step
    public static void toggleTask(String text){
        tasks.find(exactText(text)).find(".toggle").click();
    }

    @Step
    public static void toggleAll(){
        $("#toggle-all").click();
    }

    @Step
    public static void openAll(){
        $("[href='#/']").click();
    }

    @Step
    public static void openActive(){
        $("[href='#/active']").click();
    }

    @Step
    public static void openCompleted(){
        $("[href='#/completed']").click();
    }

    public static void edit(String fromText, String toText){
        actions().doubleClick(tasks.find(text(fromText)).find("label")).perform();
        tasks.find(cssClass("editing")).find(".edit").setValue(toText).pressEnter();
    }

    @Step
    public static void delete(String taskText){
        tasks.find(exactText(taskText)).hover().find(".destroy").click();
    }

    @Step
    public static void assertTasksTexts(String... texts){
        tasks.shouldHave(exactTexts(texts));
    }

    @Step
    public static void assertVisibleTaskTexts(String... texts) {
        visibleTasks.shouldHave(exactTexts(texts));
    }

    @Step
    public static void assertVisibleTasksEmpty(){
        visibleTasks.shouldBe(empty);
    }

}
