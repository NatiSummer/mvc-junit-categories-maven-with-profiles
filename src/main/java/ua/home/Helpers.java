package ua.home;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import ru.yandex.qatools.allure.annotations.Step;

public class Helpers {

    @Step
    static void assertEach(ElementsCollection elements, Condition condition){
        for (SelenideElement element: elements){
            element.shouldHave(condition);
        }
    }

}
