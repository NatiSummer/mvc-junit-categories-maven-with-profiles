package ua.home.features;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import ua.home.categories.FullAcceptance;
import ua.home.categories.Smoke;

import static ua.home.MainPage.*;

public class TodosE2ETest extends BaseTest {

    // TODO !!! -Psmoke �������� ���������� 4� ����� 2 ����, ������?

    @Category({Smoke.class, FullAcceptance.class})
    @Test
    public void lifeCycle(){
        addTasks("t1", "t2", "t3", "t4");
        assertTasksTexts("t1", "t2", "t3", "t4");
        assertItemsLeft(4);

        delete("t2");
        assertTasksTexts("t1", "t3", "t4");
        assertItemsLeft(3);

        edit("t1", "t1 - edited");
        toggleTask("t1 - edited");
        clearCompleted();
        assertTasksTexts("t3", "t4");
        assertItemsLeft(2);

        toggleTask("t3");
        assertItemsLeft(1);
        toggleTask("t3");
        assertItemsLeft(2);

        // verify filtering at Active tab
        toggleTask("t3");
        assertItemsLeft(1);
        openActive();
        assertVisibleTaskTexts("t4");
        assertItemsLeft(1);

        // verify filtering at Completed tab
        openCompleted();
        assertVisibleTaskTexts("t3");
        assertItemsLeft(1);
    }
}
