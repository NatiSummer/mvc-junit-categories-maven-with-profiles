package ua.home.features;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Screenshots;
import com.google.common.io.Files;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import ru.yandex.qatools.allure.annotations.Attachment;

import java.io.File;
import java.io.IOException;

import static com.codeborne.selenide.Selenide.executeJavaScript;
import static com.codeborne.selenide.Selenide.open;

public class BaseTest {

    public static String baseUrl;

    {
        Configuration.browser = System.getProperty("driver.browser");
        baseUrl = Configuration.baseUrl = System.getProperty("driver.baseUrl");
    }

    @BeforeClass
    public static void openTodoMVC() throws InterruptedException {
//        open(baseUrl);    // TODO: clarify, why NullPointer when use baseUrl ???
        open("http://todomvc.com/examples/troopjs_require/#");
        Thread.sleep(1000); // because of MVC realization, bad for chrome
    }

    @Before
    public void clearData() throws IOException {
        executeJavaScript("localStorage.clear()");
        open("http://todomvc.com/");
        open(baseUrl);
    }

    @After
    public void makeScreenshot() throws IOException {
        screenshot();
    }

    @Attachment(type = "image/png")
    public byte[] screenshot() throws IOException {
        File screenshot = Screenshots.getScreenShotAsFile();
        return Files.toByteArray(screenshot);
    }


/**  OLD APPROACH, with NO screenshots for reporting!
 *     @Before
 *     public void openTodoMVC() throws InterruptedException {
 *          open(baseUrl);
 *          Thread.sleep(1000); // because of MVC realization, bad for chrome
 *     }
 *
 *     @After
 *     public void clearData() {
 *          executeJavaScript("localStorage.clear()");
 *          open("http://todomvc.com/");
 *     }
 */

}
