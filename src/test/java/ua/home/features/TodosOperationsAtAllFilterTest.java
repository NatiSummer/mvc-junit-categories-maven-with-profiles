package ua.home.features;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import ua.home.categories.Buggy;
import ua.home.categories.FullAcceptance;

import static ua.home.MainPage.*;

@Category(FullAcceptance.class)
public class TodosOperationsAtAllFilterTest extends BaseTest{

    @Test
    public void testAddTasks() {
        addTasks("t1", "t2", "t3");
        assertTasksTexts("t1", "t2", "t3");
        assertItemsLeft(3);
    }

    @Test
    public void testDeleteTask() {
        addTasks("t1", "t2");
        delete("t2");
        assertTasksTexts("t1");
        assertItemsLeft(1);
    }

    @Test
    public void testMarkAsCompletedAndClear() {
        addTasks("t1", "t2");
        toggleTask("t2");
        clearCompleted();
        assertTasksTexts("t1");
        assertItemsLeft(1);
    }

    @Test
    public void testEditTask() {
        addTasks("t1");
        edit("t1", "t1 - edited");
        assertTasksTexts("t1 - edited");
    }

    @Test
    public void testReopenTask() {
        addTasks("t1", "t2");
        toggleTask("t1");
        assertItemsLeft(1);
        toggleTask("t1");
        assertItemsLeft(2);
    }

    @Test @Category(Buggy.class)
    public void testCheckingAllAndClear() {
        addTasks("t1", "t2");
        toggleAll();
        // simulated bug
        assertTasksTexts("t1", "t3");
        clearCompleted();
        assertVisibleTasksEmpty();
    }

}
